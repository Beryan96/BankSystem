﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(BankSystem.WebUI.Startup))]
namespace BankSystem.WebUI
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
