﻿using BankSystem.Domain.Abstractions;
using BankSystem.Domain.Concrete;
using System.Web.Mvc;

namespace BankSystem.WebUI.Controllers
{

    public abstract class BankRepositoryController : Controller
    {
        protected IBankRepository _repository;

        protected BankRepositoryController() : this(new BankRepository())
        {

        }

        protected BankRepositoryController(IBankRepository repository)
        {
            _repository = repository;
        }

        protected override void Dispose(bool disposing)
        {
            _repository.Dispose();
            base.Dispose(disposing);
        }
    }
}