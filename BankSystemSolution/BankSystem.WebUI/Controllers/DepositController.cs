﻿using System;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;
using BankSystem.Domain.Entities;
using BankSystem.WebUI.Models;
using Microsoft.AspNet.Identity;


namespace BankSystem.WebUI.Controllers
{
    [Authorize]
    public class DepositController : BankRepositoryController
    {
        public DepositController()
        {

        }

        // GET: Deposit

        public ActionResult Index()
        {
            return RedirectToAction("Create", "Deposit", new { area = "" });
        }

        public ActionResult Blocked()
        {
            return View();
        }
        //Calculate Profit by given values
        public string CalculateProfit(decimal money, double depositTypeInterest, int period)
        {
            decimal result = _repository.CalculateProfit(money, depositTypeInterest, period);
            return $"{result:0.00}" + " AMD";
        }

        // GET: Admin/Deposit/Create
        public ActionResult Create(double? interest, int? bankId)
        {
            DepositViewModel model = new DepositViewModel()
            {
                DepositorId = User.Identity.GetUserId()
            };
            var user = _repository.Depositors.First(d => d.Id == model.DepositorId);
            if (user.IsBlocked)
            {
                return RedirectToAction("Blocked", "Deposit", new { area = "" });
            }

            //Initialization of SelectListes for DropDownListes
            ViewBag.BanksList = new SelectList(_repository.Banks.ToList().Where(b => b.IsAble),
                nameof(Bank.Id),
                nameof(Bank.Name),
                selectedValue: _repository.Banks.FirstOrDefault(b => b.Id == bankId));
            ViewBag.DepositTypesList = new SelectList(_repository.DepositTypes.ToList(),
                nameof(DepositType.InterestRate),
                nameof(DepositType.Name));

            return View(model);
        }

        // POST: Admin/Deposit/Create         
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "BankId,DepositorId,DepositTypeInterest,Money")] DepositViewModel model)
        {

            if (ModelState.IsValid)
            {
                Deposit depo = new Deposit()
                {
                    Bank = _repository.Banks.First(b => b.Id == model.BankId),
                    Depositor = _repository.Depositors.First(d => d.Id == model.DepositorId),
                    Interest = _repository.DepositTypes.First(t => t.InterestRate == model.DepositTypeInterest).InterestRate,
                    Money = model.Money,
                    StartDate = DateTime.Now,
                    EndDate = DateTime.Now.AddDays(_repository.DepositTypes.First(t => t.InterestRate == model.DepositTypeInterest).PeriodInDays),
                    IsValid = true,
                };
                _repository.Add(depo);

                await _repository.SaveChangesAsync();

                return View("Success", depo);
            }

            ViewBag.BanksList = new SelectList(_repository.Banks.ToList().Where(b => b.IsAble), nameof(Bank.Id), nameof(Bank.Name));
            ViewBag.DepositTypesList = new SelectList(_repository.DepositTypes.ToList(), nameof(DepositType.InterestRate), nameof(DepositType.Name));

            return View(model);
        }


    }
}
