﻿using BankSystem.Domain.Entities;
using BankSystem.WebUI.Models;
using System;
using System.Globalization;
using System.Text;
using System.Linq;
using System.Web.Mvc;

namespace BankSystem.WebUI.Controllers
{
    public class HomeController : BankRepositoryController
    {
        public HomeController()
        {

        }
        //
        //Home Page
        public ActionResult Index()
        {
            ViewBag.DepositTypesList = new SelectList(_repository.DepositTypes.ToList(), nameof(DepositType.Name), nameof(DepositType.Name));

            var model = new HomePageViewModel
            {
                DepositTypes = _repository.DepositTypes.ToList(),
                Banks = _repository.Banks.Where(b => b.IsAble).ToList(),
                Depositors = _repository.Depositors.ToList()
            };

            return View(model);
        }

        public ActionResult Search(string searchBy, string searchString = "0", string depositTypeName = "", string minValue = "0", string maxValue = "0")
        {
            ViewBag.SearchBy = searchBy;
            ViewBag.SearchString = searchString;
            ViewBag.DepositTypeName = depositTypeName;
            ViewBag.MinVal = minValue;
            ViewBag.MaxVal = maxValue;
            ViewBag.DepositTypesList = new SelectList(_repository.DepositTypes.ToList(), nameof(DepositType.Name), nameof(DepositType.Name));

            var searchStr = searchString.ToLower();

            var model = new SearchViewModel()
            {
                DepositTypes = _repository.DepositTypes.ToList(),
                Depositors = _repository.Depositors.ToList()
            };

            switch (searchBy)
            {
                case "Bank":
                    model.Depositors = _repository.Deposits.ToList()
                        .Where(d => d.Bank.Name.ToLower().Contains(searchStr))
                        .Select(d => d.Depositor)
                        .Distinct().ToList();
                    return View(model);

                case "Depositor":
                    model.Depositors = _repository.Depositors
                        .Where(d => d.FullName.ToLower().Contains(searchStr))
                        .ToList();
                    return View(model);

                case "Deposit Type":
                    model.Depositors = _repository.Deposits.ToList()
                        .Where(d => d.Interest == _repository.DepositTypes.First(t => t.Name.Contains(depositTypeName)).InterestRate)
                        .Select(d => d.Depositor)
                        .Distinct().ToList();
                    return View(model);

                case "Amount":
                    decimal dMinAmount = Convert.ToDecimal(minValue);
                    decimal dMaxAmount = Convert.ToDecimal(maxValue);

                    model.Depositors = _repository.Deposits.ToList()
                        .Where(d => d.Money > dMinAmount && d.Money < dMaxAmount)
                        .Select(d => d.Depositor)
                        .Distinct().ToList();
                    return View(model);

                case "Date":
                    DateTime dFirstDate = Convert.ToDateTime(minValue);
                    DateTime dLastDate = Convert.ToDateTime(maxValue);

                    model.Depositors = _repository.Deposits.ToList()
                        .Where(d => d.StartDate > dFirstDate && d.StartDate < dLastDate)
                        .Select(d => d.Depositor)
                        .Distinct().ToList();
                    return View(model);

                default:
                    return View(model);

            }
        }
    }
}