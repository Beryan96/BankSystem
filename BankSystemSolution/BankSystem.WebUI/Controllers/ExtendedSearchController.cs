﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using BankSystem.Domain.Entities;
using BankSystem.WebUI.Models;

namespace BankSystem.WebUI.Controllers
{
    public class ExtendedSearchController : BankRepositoryController
    {
        // GET: ExtendedSearch
        public ActionResult Index()
        {
            ViewBag.DepositTypesList = new SelectList(_repository.DepositTypes.ToList(), nameof(DepositType.Name), nameof(DepositType.Name));
            return View();
        }

        public PartialViewResult ExtendedSearchPartial(string bank = "", string depositor = "", string depositTypeName = "",
            string minAmount = "", string maxAmount = "",
            string firstDate = "", string lastDate = "")
        {

            //CheckForEmptyInputs
            firstDate = firstDate == "" ? DateTime.MinValue.ToString() : firstDate;
            lastDate = lastDate == "" ? DateTime.MaxValue.ToString() : lastDate;
            minAmount = minAmount == "" ? "0" : minAmount;
            maxAmount = maxAmount == "" ? decimal.MaxValue.ToString() : maxAmount;

            bank = bank.ToLower();
            depositor = depositor.ToLower();
            decimal dMinAmount = Convert.ToDecimal(minAmount);
            decimal dMaxAmount = Convert.ToDecimal(maxAmount);
            DateTime dFirstDate = Convert.ToDateTime(firstDate);
            DateTime dLastDate = Convert.ToDateTime(lastDate);
            ViewBag.DepositTypesList = new SelectList(_repository.DepositTypes.ToList(), nameof(DepositType.Name), nameof(DepositType.Name));

            var model = new SearchViewModel
            {
                DepositTypes = _repository.DepositTypes.ToList(),

                Depositors = _repository.Deposits.ToList()
                    .Where(d => d.Bank.Name.ToLower().Contains(bank) &&  //Bank
                                d.Depositor.FullName.ToLower().Contains(depositor) && //Depositor
                                d.Interest == _repository.DepositTypes.First(t => t.Name.Contains(depositTypeName)).InterestRate && //DepositType
                                d.Money > dMinAmount && d.Money < dMaxAmount && // Amount
                                d.StartDate > dFirstDate && d.StartDate < dLastDate) //Date
                    .Select(d => d.Depositor)
                    .Distinct()
                    .ToList()
            };

            return PartialView("ExtendedSearchPartial", model);
        }
    }
}