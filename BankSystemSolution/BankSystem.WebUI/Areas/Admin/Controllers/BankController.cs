﻿using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web.Mvc;
using BankSystem.Domain.Entities;
using BankSystem.WebUI.Controllers;
using BankSystem.WebUI.Areas.Admin.Models;

namespace BankSystem.WebUI.Areas.Admin.Controllers
{
    [Authorize(Roles = "Admin")]
    public class BankController : BankRepositoryController
    {
        public BankController()
        {

        }
       
        public ActionResult Index()
        {
            var model = new BankViewModel {
                Banks = _repository.Banks.ToList()
            };
            return View(model);
        }

        // GET: Bank/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            var bank = _repository.Banks.First(b => b.Id == id);     
            
            if (bank == null)
            {
                return HttpNotFound();
            }

            var model = new BankViewModel {
                Id = bank.Id,
                Name = bank.Name,
                Address = bank.Address,
                PhoneNumber = bank.PhoneNumber,
                Email = bank.Email,
                ImageUrl = bank.ImageUrl,
                WebSite = bank.WebSite,
                IsAble = bank.IsAble
            };
            return View(model);
        }

        // GET: Bank/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Bank/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "Id,Name,Address,PhoneNumber,Email,ImageUrl,WebSite")] BankViewModel model)
        {
            if (ModelState.IsValid)
            {
                var bank = new Bank
                {
                    Id = model.Id,
                    Name = model.Name,
                    Address = model.Address,
                    PhoneNumber = model.PhoneNumber,
                    Email = model.Email,
                    ImageUrl = model.ImageUrl,
                    WebSite = model.WebSite,
                    IsAble = true
                };               
                _repository.Add(bank);
                await _repository.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            return View(model);
        }

        // GET: Bank/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            var bank = _repository.Banks.First(b => b.Id == id);

            if (bank == null)
            {
                return HttpNotFound();
            }

            var model = new BankViewModel
            {
                Id = bank.Id,
                Name = bank.Name,
                Address = bank.Address,
                PhoneNumber = bank.PhoneNumber,
                Email = bank.Email,
                ImageUrl = bank.ImageUrl,
                WebSite = bank.WebSite,
                IsAble = bank.IsAble
            };
            return View(model);
        }

        // POST: Bank/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "Id,Name,Address,PhoneNumber,Email,ImageUrl,WebSite,IsAble")] BankViewModel model)
        {
            if (ModelState.IsValid)
            {
                Bank bank = new Bank
                {
                    Id = model.Id,
                    Name = model.Name,
                    Address = model.Address,
                    PhoneNumber= model.PhoneNumber,
                    Email= model.Email,
                    ImageUrl = model.ImageUrl,
                    WebSite = model.WebSite,
                    IsAble = model.IsAble
                };
                _repository.Entry(bank).State = EntityState.Modified;

                await _repository.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            return View(model);
        }

        // GET: Bank/Disable/5
        public ActionResult Disable(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            Bank bank = _repository.Banks.First(b => b.Id == id);
            if (bank == null)
            {
                return HttpNotFound();
            }
            var model = new BankViewModel
            {
                Id = bank.Id,
                Name = bank.Name,
                Address = bank.Address,
                PhoneNumber = bank.PhoneNumber,
                Email = bank.Email,
                ImageUrl = bank.ImageUrl,
                WebSite = bank.WebSite,
                IsAble = bank.IsAble
            };
            return View(model);
        }

        // POST: Bank/Disable/5
        [HttpPost, ActionName("Disable")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DisableConfirmed(int id)
        {
            Bank bank = _repository.Banks.First(b => b.Id == id);

            if (bank != null)
            {
                bank.IsAble = false;
                await _repository.SaveChangesAsync();
            }

            return RedirectToAction("Index");
        }

        // GET: Bank/Reinstate/5
        public ActionResult Reinstate(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            Bank bank = _repository.Banks.First(b => b.Id == id);
            if (bank == null)
            {
                return HttpNotFound();
            }
            var model = new BankViewModel
            {
                Id = bank.Id,
                Name = bank.Name,
                Address = bank.Address,
                PhoneNumber = bank.PhoneNumber,
                Email = bank.Email,
                ImageUrl = bank.ImageUrl,
                WebSite = bank.WebSite,
                IsAble = bank.IsAble
            };
            return View(model);
        }

        // POST: Bank/Disable/5
        [HttpPost, ActionName("Reinstate")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> ReinstateConfirmed(int id)
        {
            Bank bank = _repository.Banks.First(b => b.Id == id);

            if (bank != null && bank.IsAble == false)
            {
                bank.IsAble = true;
                await _repository.SaveChangesAsync();
            }

            return RedirectToAction("Index");
        }

        // GET: Bank/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            Bank bank = _repository.Banks.First(b => b.Id == id);

            if (bank == null)
            {
                return HttpNotFound();
            }

            var model = new BankViewModel
            {
                Id = bank.Id,
                Name = bank.Name,
                Address = bank.Address,
                PhoneNumber = bank.PhoneNumber,
                Email = bank.Email,
                ImageUrl = bank.ImageUrl,
                WebSite = bank.WebSite,
                IsAble = bank.IsAble,
                Deposits = bank.Deposits
            };
            return View(model);
        }

        // POST: Bank/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            _repository.Remove<Bank>(id);
            await _repository.SaveChangesAsync();
            return RedirectToAction("Index");
        }

    }
}
