﻿using System;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web.Mvc;
using BankSystem.Domain.Entities;
using BankSystem.WebUI.Areas.Admin.Models;
using BankSystem.WebUI.Controllers;

namespace BankSystem.WebUI.Areas.Admin.Controllers
{
    [Authorize(Roles = "Admin")]
    public class DepositController : BankRepositoryController
    {
        public DepositController()
        {

        }
        // GET: Admin/Deposit
        public ActionResult Index()
        {
            var model = new DepositViewModel
            {
              Deposits = _repository.Deposits.ToList()
            };
            return View(model);
        }

        // GET: Admin/Deposit/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var deposit = _repository.Deposits.First(d => d.Id == id);

            if (deposit == null)
            {
                return HttpNotFound();
            }

            var model = new DepositViewModel {
                Id = deposit.Id,
                Interest = deposit.Interest,
                Money = deposit.Money,
                StartDate = deposit.StartDate,
                EndDate = deposit.EndDate,
                IsValid = deposit.IsValid
            }; 
            return View(model);
        }

        // GET: Admin/Deposit/Create
        public ActionResult Create()
        {
            DepositViewModel model = new DepositViewModel();

            //Initialization of SelectLists for DropDownLists
            ViewBag.BanksList = new SelectList(_repository.Banks.ToList().Where(b=>b.IsAble), nameof(Bank.Id), nameof(Bank.Name));
            ViewBag.DepositorsList = new SelectList(_repository.Depositors.ToList().Where(d=>d.IsBlocked == false), nameof(ApplicationUser.Id), nameof(ApplicationUser.UserName));
            ViewBag.DepositTypesList = new SelectList(_repository.DepositTypes.ToList(), nameof(DepositType.InterestRate), nameof(DepositType.Name));

            return View(model);
        }

        // POST: Admin/Deposit/Create         
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "BankId,DepositorId,Interest,Money")] DepositViewModel model)
        {

            if (ModelState.IsValid)
            {
                var depo = new Deposit()
                {
                    Bank = _repository.Banks.First(b => b.Id == model.BankId),
                    Depositor = _repository.Depositors.First(d => d.Id == model.DepositorId),
                    Interest = _repository.DepositTypes.First(t => t.InterestRate == model.Interest).InterestRate,
                    Money = model.Money,
                    StartDate = DateTime.Now,
                    EndDate = DateTime.Now.AddDays(_repository.DepositTypes.First(t => t.InterestRate == model.Interest).PeriodInDays),
                    IsValid = true
                };
                _repository.Add(depo);

                await _repository.SaveChangesAsync();

                return RedirectToAction("Index");
            }

            ViewBag.BanksList = new SelectList(_repository.Banks.ToList().Where(b => b.IsAble), nameof(Bank.Id), nameof(Bank.Name));
            ViewBag.DepositorsList = new SelectList(_repository.Depositors.ToList().Where(d => d.IsBlocked == false), nameof(ApplicationUser.Id), nameof(ApplicationUser.UserName));
            ViewBag.DepositTypesList = new SelectList(_repository.DepositTypes.ToList(), nameof(DepositType.InterestRate), nameof(DepositType.Name));

            return View(model);
        }   

        // GET: Admin/Deposit/Freeze/5
        public ActionResult Freeze(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            var deposit = _repository.Deposits.First(d => d.Id == id);

            if (deposit == null)
            {
                return HttpNotFound();
            }

            var model = new DepositViewModel
            {
                Id = deposit.Id,
                Interest = deposit.Interest,
                Money = deposit.Money,
                StartDate = deposit.StartDate,
                EndDate = deposit.EndDate,
                IsValid = deposit.IsValid
            };

            return View(model);
        }

        // POST: Admin/Deposit/Freeze/5
        [HttpPost, ActionName("Freeze")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> FreezeConfirmed(int id)
        {
            var deposit = _repository.Deposits.First(d => d.Id == id);

            if (deposit != null)
            {
                deposit.IsValid = false;
                await _repository.SaveChangesAsync();
            }

            return RedirectToAction("Index");
        }

        // GET: Admin/Deposit/Reinstate/5
        public ActionResult Reinstate(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            Deposit deposit = _repository.Deposits.First(d => d.Id == id);

            if (deposit == null)
            {
                return HttpNotFound();
            }

            var model = new DepositViewModel
            {
                Id = deposit.Id,
                Interest = deposit.Interest,
                Money = deposit.Money,
                StartDate = deposit.StartDate,
                EndDate = deposit.EndDate,
                IsValid = deposit.IsValid
            };

            return View(model);
        }

        // POST: Admin/Deposit/Reinstate/5
        [HttpPost, ActionName("Reinstate")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> ReinstateConfirmed(int id)
        {
            var deposit = _repository.Deposits.First(d => d.Id == id);

            if (deposit != null && deposit.IsValid ==false)
            {
                deposit.IsValid = true;
                await _repository.SaveChangesAsync();
            }

            return RedirectToAction("Index");
        }

    }
}
