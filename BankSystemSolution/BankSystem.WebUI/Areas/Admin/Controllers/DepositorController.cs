﻿using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web.Mvc;
using BankSystem.Domain.Entities;
using BankSystem.WebUI.Controllers;
using BankSystem.WebUI.Areas.Admin.Models;

namespace BankSystem.WebUI.Areas.Admin.Controllers
{
    public class DepositorController : BankRepositoryController
    {
        public DepositorController()
        {

        }
        
        // GET: Admin/Depositors
        public ActionResult Index()
        {
            var model = new DepositorViewModel {
                Depositors = _repository.Depositors.ToList()
            };
            return View(model);
        }

        // GET: Admin/Depositors/Details/5
        public ActionResult Details(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            var depositor = _repository.Depositors.First(d => d.Id == id);
            if (depositor == null)
            {
                return HttpNotFound();
            }

            var model = new DepositorViewModel {
                Id = depositor.Id,
                Name = depositor.Name,
                Surname = depositor.Surname,
                UserName = depositor.UserName,
                PassportID = depositor.PassportID,
                Email = depositor.Email,
                PhoneNumber = depositor.PhoneNumber,
                PhoneNumberConfirmed = depositor.PhoneNumberConfirmed,
                AccessFailedCount = depositor.AccessFailedCount,
                IsBlocked = depositor.IsBlocked
            };
            return View(model);
        }

       
        // GET: Admin/Depositors/Block/5
        public ActionResult Block(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var depositor = _repository.Depositors.First(d => d.Id == id);
            if (depositor == null)
            {
                return HttpNotFound();
            }
            var model = new DepositorViewModel
            {
                Id = depositor.Id,
                Name = depositor.Name,
                Surname = depositor.Surname,
                UserName = depositor.UserName,
                PassportID = depositor.PassportID,
                Email = depositor.Email,
                PhoneNumber = depositor.PhoneNumber,
                PhoneNumberConfirmed = depositor.PhoneNumberConfirmed,
                AccessFailedCount = depositor.AccessFailedCount,
                IsBlocked = depositor.IsBlocked
            };
            return View(model);
        }

        // POST: Admin/Depositors/Block/5
        [HttpPost, ActionName("Block")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> BlockConfirmed(string id)
        {
            var depositor = _repository.Depositors.First(d => d.Id == id);

            if (depositor != null && depositor.IsBlocked == false)
            {
                depositor.IsBlocked = true;
                await _repository.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            return RedirectToAction("Index");
        }

        public ActionResult Reinstate(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var depositor = _repository.Depositors.First(d => d.Id == id);
            if (depositor == null)
            {
                return HttpNotFound();
            }
            var model = new DepositorViewModel
            {
                Id = depositor.Id,
                Name = depositor.Name,
                Surname = depositor.Surname,
                UserName = depositor.UserName,
                PassportID = depositor.PassportID,
                Email = depositor.Email,
                PhoneNumber = depositor.PhoneNumber,
                PhoneNumberConfirmed = depositor.PhoneNumberConfirmed,
                AccessFailedCount = depositor.AccessFailedCount,
                IsBlocked = depositor.IsBlocked
            };
            return View(model);
        } 
        // POST: Admin/Depositors/Reinstate/5
        [HttpPost, ActionName("Reinstate")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> ReinstateConfirmed(string id)
        {
            ApplicationUser depositor = _repository.Depositors.First(d => d.Id == id);
            if (depositor != null && depositor.IsBlocked)
            {
                depositor.IsBlocked = false;
                await _repository.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            return RedirectToAction("Index");
        }
    }
}
