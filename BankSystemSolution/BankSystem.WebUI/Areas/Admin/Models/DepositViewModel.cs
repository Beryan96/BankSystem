﻿using BankSystem.Domain.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace BankSystem.WebUI.Areas.Admin.Models
{
    public class DepositViewModel
    {
        public int Id { get; set; }
        public int BankId { get; set; }
        public string DepositorId { get; set; }
        public double Interest { get; set; }

        [DataType(DataType.Currency)]
        public decimal Money { get; set; }

        [DataType(DataType.Date), DisplayFormat(DataFormatString = "{0:dd/mm/yy}")]
        public DateTime StartDate { get; set; }

        [DataType(DataType.Date)]
        public DateTime? EndDate { get; set; }

        public bool IsValid { get; set; }

        public IEnumerable<Deposit> Deposits { get; set; }
    }
}