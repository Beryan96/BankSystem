﻿using BankSystem.Domain.Entities;
using System.Collections.Generic;

namespace BankSystem.WebUI.Areas.Admin.Models
{
    public class BankViewModel
    {
        public int Id { get; set; }
       
        public string Name { get; set; }
        
        public string Address { get; set; }
       
        public string PhoneNumber { get; set; }
       
        public string Email { get; set; }

        public string ImageUrl { get; set; }

        public string WebSite { get; set; }

        public bool IsAble { get; set; }

        public IEnumerable<Bank> Banks{ get; set; }
        public IEnumerable<Deposit> Deposits { get; set; }

    }
}