﻿using BankSystem.Domain.Entities;
using System.Collections.Generic;

namespace BankSystem.WebUI.Areas.Admin.Models
{
    public class DepositorViewModel
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string Surname { get; set; }
        public string UserName { get; set; }
        public string PassportID { get; set; }
        public string Email { get; set; }        
        public string PhoneNumber { get; set; }
        public bool PhoneNumberConfirmed { get; set; }
        public int AccessFailedCount { get; set; }
        public bool IsBlocked { get; set; }
        
        public IEnumerable<ApplicationUser> Depositors { get; set; }
    }
}