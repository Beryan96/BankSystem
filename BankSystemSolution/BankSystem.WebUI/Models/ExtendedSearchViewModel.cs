﻿using System.Collections.Generic;
using BankSystem.Domain.Entities;

namespace BankSystem.WebUI.Models
{
    public class ExtendedSearchViewModel
    {
        public string DepositTypeName { get; set; }
        public IEnumerable<DepositType> DepositTypes { get; set; }
    }
}