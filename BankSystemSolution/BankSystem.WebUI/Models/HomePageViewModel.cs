﻿using BankSystem.Domain.Entities;
using System.Collections.Generic;

namespace BankSystem.WebUI.Models
{
    public class HomePageViewModel
    {
        public string DepositTypeName { get; set; }
        public IEnumerable<DepositType> DepositTypes { get; set; }
        public IEnumerable<Bank> Banks { get; set; }
        public IEnumerable<ApplicationUser> Depositors { get; set; }
    }
}