﻿using BankSystem.Domain.Common;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace BankSystem.WebUI.Models
{
    public class ExternalLoginConfirmationViewModel
    {
        [Required]
        [Display(Name = TextLibrary.displayEmail )]
        public string Email { get; set; }
    }

    public class ExternalLoginListViewModel
    {
        public string ReturnUrl { get; set; }
    }

    public class SendCodeViewModel
    {
        public string SelectedProvider { get; set; }
        public ICollection<System.Web.Mvc.SelectListItem> Providers { get; set; }
        public string ReturnUrl { get; set; }
        public bool RememberMe { get; set; }
    }

    public class VerifyCodeViewModel
    {
        [Required]
        public string Provider { get; set; }

        [Required]
        [Display(Name = TextLibrary.displayCode )]
        public string Code { get; set; }
        public string ReturnUrl { get; set; }

        [Display(Name = TextLibrary.displayRememberBrowser)]
        public bool RememberBrowser { get; set; }

        public bool RememberMe { get; set; }
    }

    public class ForgotViewModel
    {
        [Required]
        [Display(Name = TextLibrary.displayEmail)]
        public string Email { get; set; }
    }

    public class LoginViewModel
    {
        [Required]
        [Display(Name = TextLibrary.displayEmail)]
        [EmailAddress]
        public string Email { get; set; }

        [Required]
        [DataType(DataType.Password)]
        [Display(Name = TextLibrary.displayPassword)]
        public string Password { get; set; }

        [Display(Name = TextLibrary.displayRememberMe)]
        public bool RememberMe { get; set; }
    }

    public class RegisterViewModel
    {
        [Required]
        [EmailAddress]
        [Display(Name = TextLibrary.displayEmail)]
        public string Email { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = TextLibrary.messagePasswordLengthError, MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = TextLibrary.displayPassword)]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = TextLibrary.displayConfirmPassword)]
        [Compare("Password", ErrorMessage = TextLibrary.messagePasswordMatchError)]
        public string ConfirmPassword { get; set; }

        [Required]
        [Display(Name = TextLibrary.displayName)]
        public string Name { get; set; }

        [Required]
        [Display(Name = TextLibrary.displaySurname)]
        public string Surname { get; set; }

        [StringLength(maximumLength: 10)]
        public string PassportSeries { get; set; }

        public string PhoneNumber { get; set; }
    }

    public class ResetPasswordViewModel
    {
        [Required]
        [EmailAddress]
        [Display(Name = TextLibrary.displayEmail)]
        public string Email { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = TextLibrary.messagePasswordLengthError, MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = TextLibrary.displayPassword)]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = TextLibrary.displayConfirmPassword)]
        [Compare("Password", ErrorMessage = TextLibrary.messagePasswordMatchError)]
        public string ConfirmPassword { get; set; }

        public string Code { get; set; }
    }

    public class ForgotPasswordViewModel
    {
        [Required]
        [EmailAddress]
        [Display(Name = TextLibrary.displayEmail)]
        public string Email { get; set; }
    }
}
