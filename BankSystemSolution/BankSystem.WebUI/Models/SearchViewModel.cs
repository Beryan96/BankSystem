﻿using BankSystem.Domain.Entities;
using System.Collections.Generic;

namespace BankSystem.WebUI.Models
{
    public class SearchViewModel
    {      
        public IEnumerable<ApplicationUser> Depositors { get; set; }
        public IEnumerable<DepositType> DepositTypes { get; set; }
    }
}