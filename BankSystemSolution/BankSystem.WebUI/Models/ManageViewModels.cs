﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNet.Identity;
using Microsoft.Owin.Security;
using BankSystem.Domain.Entities;
using BankSystem.Domain.Common;

namespace BankSystem.WebUI.Models
{
    public class IndexViewModel
    {
        public string UserId { get; set; }
        public bool HasPassword { get; set; }
        public IList<UserLoginInfo> Logins { get; set; }
        public string PhoneNumber { get; set; }
        public bool TwoFactor { get; set; }
        public bool BrowserRemembered { get; set; }
        public string Name { get; set; }
        public string Surname { get; set; }
        public IEnumerable<Bank> Banks { get; set; }
        public IEnumerable<Deposit> Deposits { get; set; }
        public IEnumerable<DepositType> DepositTypes { get; set; }
        
    }

    public class ManageLoginsViewModel
    {
        public IList<UserLoginInfo> CurrentLogins { get; set; }
        public IList<AuthenticationDescription> OtherLogins { get; set; }
    }

    public class FactorViewModel
    {
        public string Purpose { get; set; }
    }

    public class SetPasswordViewModel
    {
        [Required]
        [StringLength(100, ErrorMessage = TextLibrary.messagePasswordLengthError, MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = TextLibrary.displayNewPassword)]
        public string NewPassword { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = TextLibrary.displayConfirmNewPassword)]
        [Compare("NewPassword", ErrorMessage = TextLibrary.messagePasswordMatchError)]
        public string ConfirmPassword { get; set; }
    }

    public class ChangePasswordViewModel
    {
        [Required]
        [DataType(DataType.Password)]
        [Display(Name = TextLibrary.displayCurrentPassword)]
        public string OldPassword { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = TextLibrary.messagePasswordLengthError, MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = TextLibrary.displayNewPassword)]
        public string NewPassword { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = TextLibrary.displayConfirmNewPassword)]
        [Compare("NewPassword", ErrorMessage = TextLibrary.messagePasswordMatchError)]
        public string ConfirmPassword { get; set; }
    }

    public class AddPhoneNumberViewModel
    {
        [Required]
        [Phone]
        [Display(Name = TextLibrary.displayPhoneNumber)]
        public string Number { get; set; }
    }

    public class VerifyPhoneNumberViewModel
    {
        [Required]
        [Display(Name = TextLibrary.displayCode)]
        public string Code { get; set; }

        [Required]
        [Phone]
        [Display(Name = TextLibrary.displayPhoneNumber)]
        public string PhoneNumber { get; set; }
    }

    public class ConfigureTwoFactorViewModel
    {
        public string SelectedProvider { get; set; }
        public ICollection<System.Web.Mvc.SelectListItem> Providers { get; set; }
    }
}