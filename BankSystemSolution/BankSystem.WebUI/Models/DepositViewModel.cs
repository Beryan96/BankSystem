﻿using System;
using System.ComponentModel.DataAnnotations;

namespace BankSystem.WebUI.Models
{
    public class DepositViewModel
    {
        public int BankId { get; set; }
        public string DepositorId { get; set; }
        public double DepositTypeInterest { get; set; } 
        
        [DataType(DataType.Text)]
        public decimal Money { get; set; }     

        [DataType(DataType.Date)]
        public DateTime StartDate { get; set; }

        [DataType(DataType.Date)]
        public DateTime EndDate { get; set; }
    }
}