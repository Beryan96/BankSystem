﻿$(window).bind("load", function () {
    $('#loading').fadeOut(500);
});

function scrollToBanks() { $('html, body').delay(200).animate({ scrollTop: $('#Banks_id').offset().top - 50 }, 500) };
function scrollToOffers() { $('html, body').delay(200).animate({ scrollTop: $('#Offers_id').offset().top - 50 }, 500) };
function scrollToWhyUs() { $('html, body').delay(200).animate({ scrollTop: $('#WhyUs_id').offset().top - 50 }, 500) };


function showCalculator() {
    $("#messagebox").dialog({
        height: 400,
        width: 450,
        modal: true,
        resizable: false,
    });
};

function changeSearchInputs() {
    if ($('#searchFilter option:selected').val() === 'Amount' ||
        $('#searchFilter option:selected').val() === 'Date') {

        var datePattern = "\\d{4}-\\d{1,2}-\\d{1,2}";

        $('#searchStr').addClass('hidden').val('').removeProp('required');
        $('#minval , #maxval').removeClass('hidden').prop('required', 'required');
        $('#DepositTypeName').addClass('hidden').val('').removeProp('required');

        if ($('#searchFilter option:selected').val() === 'Date') {
            $('#minval , #maxval').attr('type', 'date').attr("pattern", datePattern);
            $(function () {
                $("#minval, #maxval").datepicker({
                    dateFormat: "yy-mm-dd"
                });
            });
        } else {
            $('#minval , #maxval').attr('type', 'number').removeAttr('pattern');
            $(function () {
                $("#minval, #maxval").datepicker("destroy");
            });
        }
    } else if ($('#searchFilter option:selected').val() === 'Deposit Type') {

        $('#DepositTypeName').removeClass('hidden').prop('required', 'required');
        $('#searchStr').addClass('hidden').val('').removeProp('required');
        $('#minval , #maxval').addClass('hidden').val('').removeProp('required');

    } else {

        $('#searchStr').removeClass('hidden').prop('required', 'required');
        $('#minval , #maxval').addClass('hidden').val('').removeProp('required');
        $('#DepositTypeName').addClass('hidden').val('').removeProp('required');
    }

};

$(function () {
    $("#firstDate, #lastDate").datepicker({
        dateFormat: "yy-mm-dd"
    });
});

