﻿namespace BankSystem.Domain.Common
{
    public static class TextLibrary
    {
        public const string messageInvalidLoginError = "Invalid login attempt.";
        public const string messageInvalidCodeError = "Invalid code.";

        public const string messageChangePassword = "Your password has been changed.";
        public const string messageSetPassword = "Your password has been set.";
        public const string messageSetTwoFactor = "Your two-factor authentication provider has been set.";
        public const string messageError = "An error has occurred.";
        public const string messageAddPhone = "Your phone number was added.";
        public const string messageRemovePhone = "Your phone number was removed.";

        public const string messageSecurityCodeIs = "Your security code is: ";
        public const string messageVerifyPhoneError = "Failed to verify phone";

        public const string messageRemoveLogin = "The external login was removed.";

        public const string displayEmail = "Email";
        public const string displayCode = "Code";
        public const string displayName = "Name";
        public const string displaySurname = "Surname";
        public const string displayPhoneNumber = "Phone Number";
        public const string displayRememberBrowser = "Remember this browser?";
        public const string displayPassword = "Password";
        public const string displayConfirmPassword = "Confirm password";
        public const string displayNewPassword = "New password";
        public const string displayConfirmNewPassword = "Confirm new password";
        public const string displayCurrentPassword = "Current password";
        public const string displayRememberMe = "Remember me?";

        public const string messagePasswordLengthError = "The {0} must be at least {2} characters long.";
        public const string messagePasswordMatchError = "The password and confirmation password do not match.";

    }
}
