namespace BankSystem.Domain.Migrations
{
    using Entities;
    using Microsoft.AspNet.Identity;
    using Microsoft.AspNet.Identity.EntityFramework;
    using System.Data.Entity.Migrations;
    using System.Linq;
    using Concrete;

    public sealed class Configuration : DbMigrationsConfiguration<BankSystem.Domain.Concrete.ApplicationDbContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = true;
        }

        protected override void Seed(ApplicationDbContext context)
        {
            AddUsers(context);
            AddBanks(context);
        }

        private static void AddUsers(ApplicationDbContext context)
        {
            var manager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(context));
            var roleManager = new RoleManager<IdentityRole>(new RoleStore<IdentityRole>(context));

            if (!roleManager.RoleExists("Admin") && !roleManager.RoleExists("User"))
            {
                roleManager.Create(new IdentityRole { Name = "Admin" });
                roleManager.Create(new IdentityRole { Name = "User" });
            }

            if (context.Users.Any()) return;
            var admin = new ApplicationUser
            {
                Name = "Administrator",
                PhoneNumber = "(094) 12-69-64",
                Email = "Admin@info.am",
                UserName = "Admin@info.am",
                PassportID = "AK 0695432"
            };

            manager.Create(admin, "Admin-Password");
            manager.AddToRole(admin.Id, "Admin");
        }

        private static void AddBanks(ApplicationDbContext context)
        {
            context.Banks.AddOrUpdate(
                b => b.Email,
                new Bank
                {
                    Name = "Acba Credit Agricole Bank",
                    Address = "Erevan , Arami 82-84",
                    PhoneNumber = "(010) 31 88 88",
                    Email = "acba@acba.am",
                    ImageUrl = "http://avand.am/images/photos/akba-kredit-agrikol-bank-l.2.png",
                    WebSite = "acba.am/",
                    IsAble = true
                },
                new Bank
                {
                    Name = "ArdshinBank",
                    Address = "Erevan, Grigor Lusavorichi 13",
                    PhoneNumber = " (010) 59 23 23",
                    Email = "ashb@ardshinbank.am",
                    ImageUrl = "http://avand.am/images/photos/ardshinbank-l.7.png",
                    WebSite = "ardshinbank.am",
                    IsAble = true
                },
                new Bank
                {
                    Name = "AraratBank",
                    Address = "Erevan , Pushkini 19",
                    PhoneNumber = " (010) 59 23 23",
                    Email = "araratbank@araratbank.am",
                    ImageUrl = "http://avand.am/images/photos/araratbank-l.6.png",
                    WebSite = "araratbank.am/",
                    IsAble = true
                },
                new Bank
                {
                    Name = "ArmsvisBank",
                    Address = "Erevan , Vazgen Sargsyan 10",
                    PhoneNumber = "(060) 757 000, (011) 757 000",
                    Email = "info@armswissbank.am",
                    ImageUrl = "http://avand.am/images/photos/armsvisbank-l.8.png",
                    WebSite = "armswissbank.am/",
                    IsAble = true
                },
                new Bank
                {
                    Name = "ArtsakhBank",
                    Address = "Erevan , Charenci 1b",
                    PhoneNumber = " (060) 74-77-69, 74-77-79",
                    Email = "info@artsakhbank.am",
                    ImageUrl = "http://avand.am/images/photos/arcaxbank-l.9.png",
                    WebSite = "artsakhbank.com/",
                    IsAble = true
                },
                new Bank
                {
                    Name = "InecoBank",
                    Address = "Erevan, Tumanyan 17",
                    PhoneNumber = "(010) 51 05 10",
                    Email = "inecobank@inecobank.am",
                    ImageUrl = "http://avand.am/images/photos/inekobank-l.14.png",
                    WebSite = "inecobank.am/",
                    IsAble = true
                },
                new Bank
                {
                    Name = "ConverseBank",
                    Address = "Erevan , Vazgen Sargsyan 26/1",
                    PhoneNumber = "(010) 51 12 11",
                    Email = "post@conversebank.am",
                    ImageUrl = "http://avand.am/images/photos/konversbank-l.15.png",
                    WebSite = "conversebank.am/",
                    IsAble = true
                },
                new Bank
                {
                    Name = "ArmEconomBank",
                    Address = "Erevan, Amiryan 23/1",
                    PhoneNumber = " (010) 510 910",
                    Email = "bank@aeb.am",
                    ImageUrl = "http://avand.am/images/photos/hayekonombank-l.17.png",
                    WebSite = "aeb.am/",
                    IsAble = true
                },
                new Bank
                {
                    Name = "VTB Bank",
                    Address = "Erevan , Nalbandyan 46",
                    PhoneNumber = "(010) 56 55 78",
                    Email = "headoffice@vtb.am",
                    ImageUrl = "http://avand.am/images/photos/vtb-hayastan-bank-l.22.png",
                    WebSite = "vtb.am/",
                    IsAble = true
                }
                );
        }
    }
}
