﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using BankSystem.Domain.Entities;
using BankSystem.Domain.Abstractions;
using System.Data.Entity.Infrastructure;


namespace BankSystem.Domain.Concrete
{
    public class BankRepository : IBankRepository
    {
        public BankRepository()
        {
            InitDepositTypes();
            _context = new ApplicationDbContext();
        }

        private ApplicationDbContext _context;
        private IEnumerable<DepositType> _depositTypes;
        public IEnumerable<Bank> Banks => _context.Banks;
        public IEnumerable<DepositType> DepositTypes => _depositTypes;
        public IEnumerable<Deposit> Deposits => _context.Deposits;
        public IEnumerable<ApplicationUser> Depositors => _context.Users;
        
        public decimal CalculateProfit(decimal money, double interestRate, int period)
        {
            return money + ((money * (decimal)(interestRate / 100)) / 365) * period;
        }

        public void Add<TEntity>(TEntity entity) where TEntity : class
        {
            _context.Set<TEntity>().Add(entity);
        }

        public void Remove<TEntity>(TEntity entity) where TEntity : class
        {
            _context.Set<TEntity>().Remove(entity);
        }

        public void Remove<TEntity>(int id) where TEntity : class
        {
            TEntity entity = _context.Set<TEntity>().Find(id);

            if (entity != null)
            {
                Remove(entity);
            }
        }

        public DbEntityEntry<TEntity> Entry<TEntity>(TEntity entity) where TEntity : class
        {
            return _context.Entry(entity);
        }

        public async Task SaveChangesAsync()
        {
            await _context.SaveChangesAsync();
        }

        public void InitDepositTypes()
        {
            _depositTypes = new List<DepositType> {
                new DepositType
                    {
                        Id = 1,
                        Name = "Accumulative",
                        InterestRate = 12,                      
                        PeriodInDays = 730,
                        ImageUrl = @"/Content/Images/accumulative.png"
                    }, //Accumulative Deposit
                new DepositType
                {
                    Id = 2,
                    Name = "Family",
                    InterestRate = 13,                   
                    PeriodInDays = 300,
                    ImageUrl = @"/Content/Images/family.png"
                }, //Family Deposit
                new DepositType
                {
                    Id = 3,
                    Name = "Saving",
                    InterestRate = 9,                    
                    PeriodInDays = 546,
                    ImageUrl = @"/Content/Images/saving.png"
                } //Saving Deposit
            };
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (_context != null)
                {
                    _context.Dispose();
                    _context = null;
                }
            }
        }
    }
}
