﻿using System.Data.Entity;
using Microsoft.AspNet.Identity.EntityFramework;
using BankSystem.Domain.Entities;
using BankSystem.Domain.Migrations;

namespace BankSystem.Domain.Concrete
{
    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        public ApplicationDbContext()
            : base("DefaultConnection", throwIfV1Schema: false)
        {
             Database.SetInitializer(new MigrateDatabaseToLatestVersion<ApplicationDbContext, Configuration>());
        }

        public static ApplicationDbContext Create()
        {
            return new ApplicationDbContext();
        }

        public DbSet<Bank> Banks { get; set; }
        public DbSet<Deposit> Deposits { get; set; }
    }
}