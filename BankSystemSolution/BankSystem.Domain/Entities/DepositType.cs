﻿namespace BankSystem.Domain.Entities
{
   public class DepositType
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public double InterestRate { get; set; }        

        public int PeriodInDays { get; set; }

        public string ImageUrl { get; set; }
    }
}
