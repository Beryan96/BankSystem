﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace BankSystem.Domain.Entities
{
    public class Bank
    {
        [Key]
        public int Id { get; set; }

        [Required, StringLength(50)]
        public string Name { get; set; }

        [Required, StringLength(300)]
        public string Address { get; set; }

        [Required, StringLength(50)]
        public string PhoneNumber { get; set; }

        [EmailAddress]
        public string Email { get; set; }

        public string ImageUrl { get; set; }

        public string WebSite { get; set; }

        public bool IsAble { get; set; }
        
        public virtual ICollection<Deposit> Deposits { get; set; }
    }
}