﻿using System;
using System.ComponentModel.DataAnnotations;

namespace BankSystem.Domain.Entities
{
    public class Deposit
    {
        [Key]
        public int Id { get; set; }

        [Required]
        public double Interest { get; set; }

        [Required]
        public decimal Money { get; set; }

        [Required,DataType(DataType.Date)]              
        public DateTime StartDate { get; set; }

        [DataType(DataType.Date)]   
        public DateTime? EndDate { get; set; }

        [Required]
        public bool IsValid { get; set; }

        public virtual  Bank Bank { get; set; }

        public virtual  ApplicationUser Depositor { get; set; }
    }
}
