﻿using BankSystem.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity.Infrastructure;
using System.Threading.Tasks;

namespace BankSystem.Domain.Abstractions
{
   public interface IBankRepository : IDisposable
    {
        IEnumerable<Bank> Banks { get; }
        IEnumerable<Deposit> Deposits { get; }
        IEnumerable<ApplicationUser> Depositors { get; }
        IEnumerable<DepositType> DepositTypes { get; }
        
        decimal CalculateProfit(decimal money, double interestRate, int period);
        DbEntityEntry<TEntity> Entry<TEntity>(TEntity entity) where TEntity : class;
        void Add<TEntity>(TEntity entity) where TEntity : class;
        void Remove<TEntity>(TEntity entity) where TEntity : class;
        void Remove<TEntity>(int id) where TEntity : class;
        void InitDepositTypes();
        Task SaveChangesAsync();
        
    }
}
